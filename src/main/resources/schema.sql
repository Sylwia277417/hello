create table user
(
   id INTEGER NOT NULL,
   name VARCHAR (255) NOT NULL,
   surname VARCHAR (255) NOT NULL,
   PRIMARY KEY(id)
);

create table task
(
   id INTEGER NOT NULL,
   name VARCHAR (255) NOT NULL,
   date VARCHAR (255) NOT NULL,
   user_id INTEGER NOT NULL,
   PRIMARY KEY(id),
   FOREIGN KEY(user_id) REFERENCES user(id) ON DELETE CASCADE ON UPDATE CASCADE
);