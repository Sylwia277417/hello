package com.sylwia.app.hello.controller;

import com.sylwia.app.hello.domain.Task;
import com.sylwia.app.hello.domain.User;
import com.sylwia.app.hello.service.TaskService;
import com.sylwia.app.hello.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    TaskService taskService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List<User> getAllUsers() {

        return userService.findAll();
    }

    @RequestMapping(value = "/{userId}/task/list", method = RequestMethod.GET)
    public List<Task> getUserTasks(@PathVariable("userId") Long userId) {

        return taskService.findByUserId(userId);
    }
}
