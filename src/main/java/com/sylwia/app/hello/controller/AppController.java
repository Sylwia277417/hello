package com.sylwia.app.hello.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppController {

    private static final String HELLO = "Hello";

    @RequestMapping(value = "/hello", method = RequestMethod.GET, produces = "text/plain")
    public String getHelloPage() {

        return HELLO;
    }

}
