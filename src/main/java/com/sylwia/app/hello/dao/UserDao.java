package com.sylwia.app.hello.dao;

import com.sylwia.app.hello.domain.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserDao extends CrudRepository<User, Long> {
    List<User> findAll();
}
