package com.sylwia.app.hello.dao;

import com.sylwia.app.hello.domain.Task;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TaskDao extends CrudRepository<Task, Long> {
    List<Task> findByUserId(Long userId);
}
