package com.sylwia.app.hello.service;

import com.sylwia.app.hello.domain.User;

import java.util.List;

public interface UserService {

    List<User> findAll();
}
