package com.sylwia.app.hello.service;

import com.sylwia.app.hello.dao.TaskDao;
import com.sylwia.app.hello.domain.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    TaskDao taskDao;

    public List<Task> findByUserId(Long userId) {
        return taskDao.findByUserId(userId);
    }
}
