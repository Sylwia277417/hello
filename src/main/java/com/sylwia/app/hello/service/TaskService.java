package com.sylwia.app.hello.service;

import com.sylwia.app.hello.domain.Task;

import java.util.List;

public interface TaskService {

    List<Task> findByUserId(Long userId);
}
