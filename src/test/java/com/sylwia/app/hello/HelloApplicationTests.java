package com.sylwia.app.hello;

import com.sylwia.app.hello.domain.Task;
import com.sylwia.app.hello.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class HelloApplicationTests {

	private static final String HELLO = "Hello";

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void checkGetHelloStringMethodResponse() {
		ResponseEntity<String> getResponse = restTemplate.getForEntity("/hello", String.class);
		String hello = getResponse.getBody();

		assertEquals(HELLO, hello);
	}

	@Test
	public void checkGetUsersMethodResponse() {
		ResponseEntity<User[]> getResponse = restTemplate.getForEntity("/user/list", User[].class);
		User[] users = getResponse.getBody();

		assertEquals(4, users.length);
	}

	@Test
	public void checkGetTasksMethodResponse() {
		ResponseEntity<Task[]> getResponse = restTemplate.getForEntity("/user/2/task/list", Task[].class);
		Task[] tasks = getResponse.getBody();

		assertEquals(2, tasks.length);
	}
}
